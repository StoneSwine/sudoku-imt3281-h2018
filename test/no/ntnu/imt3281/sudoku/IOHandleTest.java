package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;


import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.Test;

import no.ntnu.imt3281.sudoku.enums.Difficulty;

import java.util.Arrays;


public class IOHandleTest {

	
	/*@Test
	public void getSudokuBoardTest() {
		IOHandle handler = new IOHandle();
		
	}*/
	
	
	
	
	
	@Test
	public void getSudokuBoardTest() {
		//final int[] expected = {5, 3, -1,6, -1, -1,-1, 9, 8,-1, 7, -1,1, 9, 5,-1, -1, -1,-1, -1, -1,-1, -1, -1,-1, 6, -1,8, -1, -1,4, -1, -1,7, -1, -1,-1, 6, -1,8, -1, 3,-1, 2, -1,-1, -1, 3,-1, -1, 1,-1, -1, 6,-1, 6, -1,-1, -1, -1,-1, -1, -1,-1, -1, -1,4, 1, 9, -1, 8, -1,2, 8, -1,-1, -1, 5,-1, 7, 9};
		final int[] expected = {5, 3, -1, -1, 7, -1, -1, -1, -1, 6, -1, -1, 1, 9, 5, -1, -1, -1, -1, 9, 8, -1, -1, -1, -1, 6, -1, 8, -1, -1, -1, 6, -1, -1, -1, 3, 4, -1, -1, 8, -1, 3, -1, -1, 1, 7, -1, -1, -1, 2, -1, -1, -1, 6, -1, 6, -1, -1, -1, -1, 2, 8, -1, -1, -1, -1, 4, 1, 9, -1, -1, 5, -1, -1, -1, -1, 8, -1, -1, 7, 9};
		IOHandle ioh = new IOHandle(Difficulty.SIMPLE);
		try {
		int[] boardarr = ioh.getSudokuBoard();
		
		assertArrayEquals(expected, boardarr);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * this is usually a private function.
	 * Set to public only for testing the function.
	 * Will be set to private when the functionality and behavior is validated
	 * 
	 * So don't expect this to run later
	 */
	
	/*
	@Test public void getSudokuDataTest() {	
		/*resultArr is not correct. Fix this later*/
	/*	final int[] resultArr = {5, 3, -1, 6, -1, -1,-1, 9, 8,-1, 7, -1,1, 9, 5,-1, -1, -1,-1, -1, -1-1, -1, -1-1, 6, -1 ,8, -1, -1,4, -1, -1,7, -1, -1,-1, 6, -1,8, -1, 3,-1, 2, -1,-1, -1, 3,-1, -1, 1,-1, -1, 6, -1, 6, -1,-1, -1, -1,-1, -1, -1,-1, -1, -1,4, 1, 9,-1, 8, -1,2, 8, -1-1, -1, 5,-1, 7, 9};
		System.out.println(resultArr.length);
		int[] result;
		
		IOHandle handler = new IOHandle();
		JSONArray board;
		try {
		board = handler.getSudokuData("resources/simpleBoard.json");
		//System.out.println(board.toString());
		result = handler.convertJsonToArray(board);
		
		System.out.println(Arrays.toString(resultArr));
		System.out.println(Arrays.toString(result));
		assertArrayEquals(resultArr, result);
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	*/
	
	
	/*
	 * Array that simpleJson should be converted to
	 * {5, 3, -1,6, -1, -1,-1, 9, 8,-1, 7, -1,1, 9, 5,-1, -1, -1,-1, -1, -1-1, -1, -1-1, 6, -1 ,8, -1, -1,4, -1, -1,7, -1, -1,-1, 6, -1,8, -1, 3,-1, 2, -1,-1, -1, 3,-1, -1, 1,-1, -1, 6, -1, 6, -1,-1, -1, -1,-1, -1, -1,-1, -1, -1,4, 1, 9,-1, 8, -1,2, 8, -1-1, -1, 5,-1, 7, 9}
	 * */

	
	/*@Test
	public void readSudokuSetupTest() throws Exception {
		final int [] expectedResult = new int [] {1,2,3,4};
		IOHandle handler = new IOHandle();
		int [] sudokuStructure = handler.readSudokuSetup("simpleBoard.json");
		assertArrayEquals(expectedResult, sudokuStructure );
	}*/

}
