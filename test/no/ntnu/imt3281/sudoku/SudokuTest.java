package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;
import java.util.Arrays;

import org.junit.Test;

import no.ntnu.imt3281.sudoku.enums.Difficulty;

public class SudokuTest {

	
	/**
	 * Supposed to test the input of sudoku numbers to the board.
	 * 
	 * The idea is to test valid input and wrong input 
	 * 
	 */
	
	@Test
	public void setSudokuValueInCellSuccessTest() {
		final int[] solution = {5, 3, 4, 6, 7, 8, 9, 1, 2,
				6, 7, 2, 1, 9, 5, 3, 4, 8,
				1, 9, 8, 3, 4, 2, 5, 6, 7,
				8, 5, 9, 7, 6, 1, 4, 2, 3,
				4, 2, 6, 8, 5, 3, 7, 9, 1,
				7, 1, 3, 9, 2, 4, 8, 5, 6,
				9, 6, 1, 5, 3, 7, 2, 8, 4,
				2, 8, 7, 4, 1, 9, 6, 3, 5,
				3, 4, 5, 2, 8, 6, 1, 7, 9
				};
		int[] testInput = {
				-1, -1, 4, 6, -1, 8, 9, 1, 2,
				-1, 7, 2, -1, -1, -1, 3, 4, 8,
				1, -1, -1, 3, 4, 2, 5, -1, 7,
				-1, 5, 9, 7, -1, 1, 4, 2, -1,
				-1, 2, 6, -1, 5, -1, 7, 9, -1,
				-1, 1, 3, 9, -1, 4, 8, 5, -1,
				9, -1, 1, 5, 3, 7, -1, -1, 4,
				2, 8, 7, -1, -1, -1, 6, 3, -1,
				3, 4, 5, 2, -1, 6, 1, -1, -1
				};
		
		try {
			Sudoku board = new Sudoku(Difficulty.SIMPLE);
			board.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			
			for(int i = 0; i<testInput.length; i++) {
				if(testInput[i] > -1) {
					try {
					board.setSudokuValueInCell(i, testInput[i]);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			//After inserting correct values we check if the sudoku.board array is equal our solution.
			//System.out.println(Arrays.toString(board.getBoard()));
			assertArrayEquals(solution, board.getBoard());
		}catch(Exception e) {
			//e.printStackTrace();
		}
		
		//	
	}
	
	@Test
	public void setSudokuValueInCellFailureTest() {
		final int[] solution = {
				5, 3, 4, 6, 7, 8, 9, 1, 2,
				6, 7, 2, 1, 9, 5, 3, 4, 8,
				1, 9, 8, 3, 4, 2, 5, 6, 7,
				8, 5, 9, 7, 6, 1, 4, 2, 3,
				4, 2, 6, 8, 5, 3, 7, 9, 1,
				7, 1, 3, 9, 2, 4, 8, 5, 6,
				9, 6, 1, 5, 3, 7, 2, 8, 4,
				2, 8, 7, 4, 1, 9, 6, 3, 5,
				3, 4, 5, 2, 8, 6, 1, 7, 9
		};
		int[] testInput = {-1, -1, 4, -1, 7, 2, 1, -1, -1, 6, -1, 8, -1, -1, -1, 3, 4, 2, 9, 1, 2, 3, 4, 8, 5, -1, 7, -1, 5, 9, -1, 2, 6, -1, 1, 3, 7, -1, 1, -1, 5, -1, 9, -1, 4, 4, 2, -1, 7, 9, -1, 8, 5, -1, 9, -1, 1, 2, 8, 7, 3, 4, 5, 5, 3, 7, -1, -1, -1, 2, -1, 6, -1, -1, 4, 6, 3, -1, 1, -1, -1
				};
		
		try {
			Sudoku board = new Sudoku(Difficulty.SIMPLE);
			board.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			
			for(int i = 0; i<testInput.length; i++) {
				if(testInput[i] > -1) {
					try {
					board.setSudokuValueInCell(i, testInput[i]);
					}catch(Exception e) {
						//e.printStackTrace();
					}
				}
			}
			
			//After inserting correct values we check if the sudoku.board array is equal our solution.
			//System.out.println(Arrays.toString(board.getBoard()));
			//assertArray
			
			assertFalse(Arrays.equals(solution, board.getBoard()));
			
			//assertArrayEquals(solution, board.getBoard());
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		//	
	}
	/**
	 * Runs test to see if validation of a block works with correct input
	 */
	@Test
	public void validateInsertInBlock() {
		final int[] korrekt = {-1, -1, 4, 6, -1, 8, 9, 1, 2, -1, 7, 2, -1, -1, -1, 3, 4, 8, 1, -1, -1, 3, 4, 2, 5, -1, 7, -1, 5, 9, 7, -1, 1, 4, 2, -1, -1, 2, 6, -1, 5, -1, 7, 9, -1, -1, 1, 3, 9, -1, 4, 8, 5, -1, 9, -1, 1, 5, 3, 7, -1, -1, 4, 2, 8, 7, -1, -1, -1, 6, 3, -1, 3, 4, 5, 2, -1, 6, 1, -1, -1};
		
		try {
			Sudoku board = new Sudoku(Difficulty.SIMPLE);
			
			for(int i = 0; i<korrekt.length; i++) {
				if(korrekt[i] > -1) {
					try {
						assertTrue(board.validateCellInSquareAtIndex(i, korrekt[i]));
						//board.setSudokuValueInCell(i, korrekt[i]);
					}catch(Exception e) {
						//e.printStackTrace();
					}
				}
			}
		}catch(Exception e) {
			System.out.println("Validate block success, failed: {0}".format(e.getMessage()));
		}

	}
	
	
	
	/*@Test
	public void testEmptyConstructor() {
		Sudoku sudoku = new Sudoku();
		assertTrue(sudoku instanceof Sudoku);
	}*/
	
	@Test
	public void mirrorHorizontalTest() {
		final int[] correctMirror = {
				-1, -1, -1, -1, 8, -1, -1, 7, 9, 
				-1, -1, -1, 4, 1, 9, -1, -1, 5,
				-1, 6, -1, -1, -1, -1, 2, 8, -1,
				7, -1, -1, -1, 2, -1, -1, -1, 6,
				4, -1, -1, 8, -1, 3, -1, -1, 1, 
				8, -1, -1, -1, 6, -1, -1, -1, 3, 
				-1, 9, 8, -1, -1, -1, -1, 6, -1,
				6, -1, -1, 1, 9, 5, -1, -1, -1, 
				5, 3, -1, -1, 7, -1, -1, -1, -1};
		try {
			Sudoku su = new Sudoku(Difficulty.HARD);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			su.mirrorHorizontal();
			assertArrayEquals(correctMirror, su.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void mirrorVerticalTest() {
		final int[] correctMirror = {
				-1, -1, -1, -1, 7, -1, -1, 3, 5, 
				-1, -1, -1, 5, 9, 1, -1, -1, 6, 
				-1, 6, -1, -1, -1, -1, 8, 9, -1, 
				3, -1, -1, -1, 6, -1, -1, -1, 8, 
				1, -1, -1, 3, -1, 8, -1, -1, 4, 
				6, -1, -1, -1, 2, -1, -1, -1, 7, 
				-1, 8, 2, -1, -1, -1, -1, 6, -1, 
				5, -1, -1, 9, 1, 4, -1, -1, -1, 
				9, 7, -1, -1, 8, -1, -1, -1, -1};
		try {
			Sudoku su = new Sudoku(Difficulty.HARD);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			su.mirrorVertical();
			assertArrayEquals(correctMirror, su.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void mirrorDescendingDiagonalTest() {
		final int[] correctMirror = {
				5, 6, -1, 8, 4, 7, -1, -1, -1, 
				3, -1, 9, -1, -1, -1, 6, -1, -1, 
				-1, -1, 8, -1, -1, -1, -1, -1, -1, 
				-1, 1, -1, -1, 8, -1, -1, 4, -1, 
				7, 9, -1, 6, -1, 2, -1, 1, 8, 
				-1, 5, -1, -1, 3, -1, -1, 9, -1, 
				-1, -1, -1, -1, -1, -1, 2, -1, -1, 
				-1, -1, 6, -1, -1, -1, 8, -1, 7, 
				-1, -1, -1, 3, 1, 6, -1, 5, 9
		};
		try {
			Sudoku su = new Sudoku(Difficulty.HARD);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			su.mirrorDescendingDiagonal();
			assertArrayEquals(correctMirror, su.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
 	@Test
	public void mirrorAscendingDiagonalTest() {
		final int[] correctMirror = {
				9, 5, -1, 6, 1, 3, -1, -1, -1, 
				7, -1, 8, -1, -1, -1, 6, -1, -1, 
				-1, -1, 2, -1, -1, -1, -1, -1, -1, 
				-1, 9, -1, -1, 3, -1, -1, 5, -1, 
				8, 1, -1, 2, -1, 6, -1, 9, 7, 
				-1, 4, -1, -1, 8, -1, -1, 1, -1, 
				-1, -1, -1, -1, -1, -1, 8, -1, -1,
				-1, -1, 6, -1, -1, -1, 9, -1, 3, 
				-1, -1, -1, 7, 4, 8, -1, 6, 5
		};
		try {
			Sudoku su = new Sudoku(Difficulty.HARD);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			su.mirrorAscendingDiagonal();
			assertArrayEquals(correctMirror, su.getBoard());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void validateCellInRowAtIndexTest() {
		try {
			Sudoku su = new Sudoku(Difficulty.HARD);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			assertTrue(su.validateCellInRowAtIndex(10, 4));
			assertFalse(su.validateCellInRowAtIndex(11, 6));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void validateCellInColumnAtIndexTest() {
		try {
			Sudoku su = new Sudoku(Difficulty.SIMPLE);
			su.setBoard(new int[] {
					5, 3, -1, -1, 7, -1, -1, -1, -1, 
					6, -1, -1, 1, 9, 5, -1, -1, -1, 
					-1, 9, 8, -1, -1, -1, -1, 6, -1, 
					8, -1, -1, -1, 6, -1, -1, -1, 3, 
					4, -1, -1, 8, -1, 3, -1, -1, 1, 
					7, -1, -1, -1, 2, -1, -1, -1, 6, 
					-1, 6, -1, -1, -1, -1, 2, 8, -1, 
					-1, -1, -1, 4, 1, 9, -1, -1, 5, 
					-1, -1, -1, -1, 8, -1, -1, 7, 9
			});
			assertTrue(su.validateCellInColumnAtIndex(13, 5));
			assertFalse(su.validateCellInColumnAtIndex(22, 6));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}