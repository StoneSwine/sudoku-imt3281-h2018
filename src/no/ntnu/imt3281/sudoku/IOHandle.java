package no.ntnu.imt3281.sudoku;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;

import no.ntnu.imt3281.sudoku.enums.Difficulty;



/**
 * <h1>IOHandle - read sudoku board from filesystem</h1>
 * Used to handle suodu boards located on the filesystem
 * It can handle multiple sudokufiles with difficulty which it reads from a properties file at initialization.
 * 
 * 
 * @author joakimellestad
 * @since 2018.09.10
 * @version 0.5
 * 
 */
class IOHandle {
	private static final Logger LOGGER = Logger.getLogger( IOHandle.class.getName() );
	private String sudokuFile;
	

	/**
	 * Takes the difficulty level and reads in the properties file and determines what file it should used further on.
	 * IOHandle is a use once and discard class. No need to have this stored in memory when the user is going to be 
	 * scratching his head trying to solve sudoku.
	 * @param level is of type difficulty to find the correct sudoku file
	 * 
	 */
	IOHandle(Difficulty level) {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("resources/sudokuFiles.properties");
			prop.load(input);
		} catch(IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e );
		}finally {
			try {
				assert input != null;
				input.close();
			} catch (IOException | NullPointerException e) {
				LOGGER.log(Level.SEVERE, e.toString(), e);
			}
		}

		switch(level) {
		case SIMPLE:
			sudokuFile = prop.getProperty("easy");
			break;
		case MEDIUM:
			sudokuFile = prop.getProperty("medium");
			break;
		case HARD:
			sudokuFile = prop.getProperty("hard");
			break;
		default:
			throw new IllegalArgumentException("Invalid parameter: " + level);
		}

	}

	/**
	 * The only method that should be needed by the Sudoku class to get it's board
	 * 
	 * @return int[]
	 * @throws Exception
	 */
	int [] getSudokuBoard() throws Exception {
		JSONArray jsonData;
		try {
			jsonData = getSudokuData(this.sudokuFile);
			/*Hope the data is valid*/
			
			return convertJsonToArray(jsonData);
		}catch(Exception e){
			LOGGER.log( Level.SEVERE, e.toString(), e );
		}
		throw new Exception("Could not get sudoku board");
	}
	
	/**
	 * 
	 * Reads a resource file given
	 * Exception should be handled by caller.
	 * @param resourceFile
	 * @return JSONAray 
	 * @throws Exception
	 */
	private JSONArray getSudokuData(String resourceFile ) throws Exception {
		try( BufferedReader br = new BufferedReader (new FileReader(resourceFile))){
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine())!=null) {
				sb.append(line);
			}
			/*Check that our json is valdi before returning*/
			return new JSONArray(sb.toString());
		} catch(IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e );
		}
		throw new Exception("Failed getting sudokudata from file");
	}
	
	/**
	 * Takes a jsonarray
	 * each object/row will be put in the array in the same order
	 * Loops data and adds it to array
	 * @param data
	 * @return
	 */
	private int[] convertJsonToArray(JSONArray data) {
		int[] tmpSudokuBoard = new int[81];
		
		int index = 0;
		for(Object obj : data ) {
			JSONArray arr = (JSONArray) obj;
			for(int i = 0; i < arr.length(); i++) {
				tmpSudokuBoard[index++] = arr.optInt(i);
			}
		}
		return tmpSudokuBoard;
	}
	
	/*
	 * Takes a json object and converts it to array
	 */
	
	/*
	 * 
	 *[
	 * [5, 3, -1, -1, 7, -1, -1, -1, -1],
	 * [6, -1, -1, 1, 9, 5, -1, -1, -1], 
	 * [-1, 9, 8, -1, -1, -1, -1, 6, -1], 
	 * [8, -1, -1, -1, 6, -1, -1, -1, 3], 
	 * [4, -1, -1, 8, -1, 3, -1, -1, 1], 
	 * [7, -1, -1, -1, 2, -1, -1, -1, 6], 
	 * [-1, 6, -1, -1, -1, -1, 2, 8, -1], 
	 * [-1, -1, -1, 4, 1, 9, -1, -1, 5], 
	 * [-1, -1, -1, -1, 8, -1, -1, 7, 9]
	 *]
	 * 		
	 */
}







































