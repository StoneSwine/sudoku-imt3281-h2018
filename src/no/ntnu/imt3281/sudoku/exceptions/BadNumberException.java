package no.ntnu.imt3281.sudoku.exceptions;

/**
 * Extends in order to throw a detailed explenation of usage of sudoku numbers
 * 
 * @author Unknown?
 *
 */
public class BadNumberException extends Exception{

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private int badNumber;

	    public BadNumberException(int number, String message)
	    {
	        super(message); 
	        badNumber = number; 
	    }

	    public BadNumberException( )
	    {
	        super("BadNumberException"); 
	    }

	    public BadNumberException(String message)
	    {
	        super(message); 
	    }

	    public int getBadNumber( )
	    {
	        return badNumber;
	    }

	    @Override
	    public String toString() {
	    String s = getClass().getName();
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message + "badNumber: " + this.badNumber) : s;
	    }
}
