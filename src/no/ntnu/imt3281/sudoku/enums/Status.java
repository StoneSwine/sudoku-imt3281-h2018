package no.ntnu.imt3281.sudoku.enums;


//Enum for each cell's status, each enum has been given a numeric value which Joakim has claimed will prove useful
public enum Status { 
	LOCKED(-1),
	EMPTY(0),
	CORRECT(1),
	WRONG(2);
		
	
	private int statusValue;	//Variable to set the numeric value

	
	//Sets an enums numeric value
	Status(int statusValue) {
		this.statusValue = statusValue;
	}
	
	
	//Gets the numeric value of the enum
	public int getStatusValue() {
		return statusValue;
	}
}