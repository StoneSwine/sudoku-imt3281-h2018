package no.ntnu.imt3281.sudoku.enums;

/*
 * Difficulty for the sudoku board.
 * 
 */
public enum Difficulty {
	SIMPLE,
	MEDIUM,
	HARD;
	
	@Override
	public String toString() {
		switch(this) {
		case SIMPLE:
			return "Simple";
		case MEDIUM:
			return "Medium";
		case HARD:
			return "Hard";
		default: throw new IllegalArgumentException();
		}
	}
	
}
