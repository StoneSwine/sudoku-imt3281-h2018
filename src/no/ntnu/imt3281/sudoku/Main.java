package no.ntnu.imt3281.sudoku;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ResourceBundle;

/**
 * The starting place for the Sudoku application
 * 
 * Starts of by loading the only view; PlayBoardView.fxml
 * This is handled by the controller "Controller.java"
 * 
 * @author Joakim Ellestad, Magnus Lilja, Bendik Flobak
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception{
        ResourceBundle bundle = ResourceBundle.getBundle("settings");
		Parent root = FXMLLoader.load(getClass().getResource("PlayBoardView.fxml"), bundle);

		Scene scene = new Scene(root, 470, 500);
		stage.setTitle("Sudoku");
		stage.setScene(scene);
		stage.show();
	}
	
    public static void main(String[] args) {
        launch(args);
    }
}