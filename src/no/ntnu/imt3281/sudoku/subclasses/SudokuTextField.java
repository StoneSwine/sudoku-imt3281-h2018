package no.ntnu.imt3281.sudoku.subclasses;

import javafx.scene.control.TextField;

/**
 * Subclassed to hold extra information. Could probably use userdata but this way is easier
 * 
 * row stores the row position 0-9
 * column stores the column position 0-9
 * Index holds the index in board array
 * the index can be derived from using the row and column.
 * @author Joakim Ellestad
 *
 */
public class SudokuTextField extends TextField{
	/**
	 * can hold 0-9.
	 */
	private int row;
	/**
	 * can hold 0-9.
	 */
	private int column;
	/**
	 * Can hold an int if you want
	 */
	private int index;
	
	/**
	 * Holds a boolean that determines if a user has earlier entered a wrong value, but can later be valid
	 * if other values on the board gets cleared
	 */
	private Boolean hasCandidateValue;
	
	
	/**
	 * Initializes this class
	 * Importantly calls super so that can actually create a textfield.
	 * 
	 * @param row
	 * @param column
	 * @param tag
	 */
	public SudokuTextField(int row, int column, int index){
		super();
		this.row = row;
		this.column = column;
		this.index = index;
		this.hasCandidateValue = false;
	}
	
	public int getRow() {
		return this.row;
	}
	public int getColumn() {
		return this.column;
	}
	public int getIndex() {
		return this.index;
	}
	
	public void setCandidateValueTrue() {
		this.hasCandidateValue = true;
	}
	public void setCandidateValueFalse() {
		this.hasCandidateValue = false;
	}
	public  Boolean getCandidateValue() {
		return this.hasCandidateValue;
	}
	
}
