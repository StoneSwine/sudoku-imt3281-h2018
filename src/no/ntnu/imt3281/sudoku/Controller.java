package no.ntnu.imt3281.sudoku;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.util.Duration;
import no.ntnu.imt3281.sudoku.exceptions.BadNumberException;
import no.ntnu.imt3281.sudoku.enums.Difficulty;
import no.ntnu.imt3281.sudoku.subclasses.SudokuTextField;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the controller that connects the model with the view.
 * Controller takes controll and presents the sudoku board graphics.
 * Interactions to the sudoku board graphics is handled by this and passed on to the sudoku class if necessary.
 * 
 * 
 * 
 * @author Joakim Ellestad, Magnus Lilja, Bendik Flobak
 *
 */
public class Controller {
    //Logger object to print logging messages
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    @FXML
    private StackPane layout;

    @FXML
    private BorderPane borderpane;

    @FXML
    private ToggleButton toggleEasy;

    @FXML
    private ToggleButton toggleMedium;

    @FXML
    private ToggleButton toggleHard;

    @FXML
    private Label feedbackLabel;

    private ToggleGroup toggleGroup;

    /*This MUST be named resources. resources is injected since JavaFX 2.0*/
    @FXML
    private ResourceBundle resources;

    private static final String NOTVALID = "notvalid";

    private Difficulty level;

    private Sudoku sudoku;
    
    // To avoid triggering listener during setup of the game. E.g. when we instert the board we dont
    // want the listeners to trigger, because we have control of what we insert. hopefully
    private Boolean gamePrep;
    
    /* Variable for the rotation of the board */
    int rotation = 0; 
    
    private static final int SUDOKULENGTH = 81;

    //An array of listeners: needed to remove them temporary from the field
    private ChangeListener<String>[] listeners = new ChangeListener[SUDOKULENGTH];
    //Array of textfields
    private SudokuTextField[] textfieldarray = new SudokuTextField[SUDOKULENGTH];

    
    /**
     * This is being called at creation automagically :)
     * Initalization for a controller
     * Load properties file so we can deliver correct language support
     */
    @FXML
    private void initialize() {
        /*
         * Setting up the toggle group
         */
        this.toggleGroup = new ToggleGroup();
        //Set our toggles to our group - easier to manage

        //Easy toggle
        this.toggleEasy.setToggleGroup(this.toggleGroup);
        this.toggleEasy.setUserData(Difficulty.SIMPLE);
        this.level = Difficulty.SIMPLE;
        this.toggleGroup.selectToggle(this.toggleEasy);
        this.feedbackLabel.setText(resources.getString("neweasy"));
        //Medium toggle
        this.toggleMedium.setToggleGroup(this.toggleGroup);
        this.toggleMedium.setUserData(Difficulty.MEDIUM);
        //Hard toggle
        this.toggleHard.setToggleGroup(this.toggleGroup);
        this.toggleHard.setUserData(Difficulty.HARD);
        //Lets set the easy to default

        this.setup();


    }

    /**
     * Changes the difficulty
     * New board is created using another button, though it could be done here.
     * @param event
     */
    @FXML
    void toggleDifficulty(ActionEvent event) {
        //Could also get this from the event. But hey, lets see if this works
        ToggleButton toggle = (ToggleButton) this.toggleGroup.getSelectedToggle();
        Difficulty selected = (Difficulty) toggle.getUserData();

        this.level = selected;

        String feedback = "Ups, error";
        switch (this.level) {
            case SIMPLE:
                feedback = resources.getString("neweasy");
                rotation = 0;
                break;
            case MEDIUM:
                feedback = resources.getString("newmedium");
                rotation = 0;
                break;
            case HARD:
                feedback = resources.getString("newhard");
                rotation = 0;
                break;
            default:
                feedback += "...did monkeys code this?"; //Just for laugh. We can code...right? ...guys?
        }

        this.feedbackLabel.setText(feedback);
    }

    /**
     * Make a new board
     * from this.level we know what to load
     *
     * @param event
     */
    @FXML
    void newBoard(ActionEvent event) {
        this.gamePrep = true;
        try {
            sudoku = new Sudoku(this.level);
            setBoard();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        insertBoard();
        //initializeListeners();
        this.gamePrep = false;
    }
    
    
    /**
     * Sets the boards mirroring, 
     * each difficulty with start with the regular board and go through the mirrors in a sequential order
     */
    void setBoard() {
    	switch (rotation%5) {
		case 1:
			sudoku.mirrorHorizontal();
			break;
		case 2:
			sudoku.mirrorVertical();
			break;
		case 3:
			sudoku.mirrorAscendingDiagonal();
			break;
		case 4:
			sudoku.mirrorDescendingDiagonal();
			break;
		default:
			break;
		}
    	rotation++;
    }

    /**
     * Initializing the textfieldarray before the GUI is displayed
     * Structure:
     * Borderpane(Center) --> outergridpane --> grid --> tmptextfield
     * <p>
     * ****Coordinate logic*********
     * The datastructure we are using is a one dimensional array
     * where the first 1-9 is a row, NOT a block as one might think.
     * This means that for generating the correct rowCoordinate, columnCoordinate and index
     * values that we want to be stored in each SudokuTextField` we have to do some tricks;
     * <p>
     * The looping here creates one block at a time going from top left,
     * towards right, then the next block row, then the last block row.
     * <p>
     * rowCoordinat:
     * Since boxrows goes from 0->2 each block starting point will be boxrows*3.
     * This gives us: 0 + rows, 3+rows, 6 + rows
     * <p>
     * columnCoordinate:
     * The same is for columns
     * This gives us: 0 + cols, 3 + cols, 6 + cols
     * <p>
     * index:
     * The index is used to set the textfield to it's appropriate space in the textfield array. That is where we store them.
     * It actually doesn't matter where we store them since each textfield holds on to what it is.
     * <p>
     * But...we do it.
     * <p>
     * Event though we not necessary need to store index in the textfield since coordinates is enough we do that also...
     * It might be more efficient to store it instead of calculating it each time an index needs to be
     * used with the Sudoku class.
     * <p>
     * The index goes from 0 -> 9 for one row in a sudoku board.
     * This means that the rowCoordinate and columnCoordinate can be used to calculate the index.
     * Since rowCoordinate gives the row we multiply it with 9 to cover the whole row. A row is of 9 elements.
     * Then if we add the columnCoordinate we know where in the row we are.
     * That give us the index.
     * <p>
     * <p>
     * If you are confused of the setup ask Joakim for the play file that generates coordinates, index,
     * reverse engineer the column and row from a index.
     */
    private void setup() {

        //counter to map the textfield into the right arrayposition
        // new 3x3 gridpane for the whole board
        GridPane outergridpane = new GridPane();
        int numberofboxes = 3; // 3x3 board
        //loop through each of the main squares
        for (int boxrows = 0; boxrows < numberofboxes; boxrows++) {
            for (int boxcols = 0; boxcols < numberofboxes; boxcols++) {
                //for each square: make a new gridpane with a border
                GridPane grid = new GridPane();
                grid.setStyle("-fx-background-color: black, -fx-control-inner-background; -fx-background-insets: 0, 1; -fx-padding: 1;");
                //for each of the inner gridpane squares, make a new textfield with some style
                for (int rows = 0; rows < numberofboxes; rows++) {
                    for (int cols = 0; cols < numberofboxes; cols++) {

                        int rowCoordinate = (boxrows * 3) + rows;
                        int columnCoordinate = (boxcols * 3) + cols;
                        int index = (rowCoordinate * 9) + columnCoordinate;
                        SudokuTextField tmpTextField = new SudokuTextField(rowCoordinate, columnCoordinate, index);

                        GridPane.setMargin(tmpTextField, new Insets(3, 3, 3, 3));
                        GridPane.setHgrow(tmpTextField, Priority.ALWAYS);
                        GridPane.setVgrow(tmpTextField, Priority.ALWAYS);

                        GridPane.setConstraints(tmpTextField, cols, rows);
                        grid.getChildren().add(tmpTextField);
                        //Move each of the textfields into a array for easier access
                        tmpTextField.textProperty().addListener((observable, oldValue, newValue) -> numberSubmitted(index, oldValue, newValue));
                        textfieldarray[index] = tmpTextField;
                    }

                }
                //style
                GridPane.setHgrow(grid, Priority.ALWAYS);
                GridPane.setVgrow(grid, Priority.ALWAYS);
                GridPane.setConstraints(grid, boxcols, boxrows);
                outergridpane.getChildren().add(grid);

            }
        }                  //Initialize the listeners
        //this.initializeListeners();
        //attach the outer gridpane to the borderpane
        this.borderpane.setCenter(outergridpane);

    }


    /**
     * This is triggered when a change happens in a textfield
     * The user might: set number, remove number
     * Either the possibilities we need to reset the style, since it might have been red.
     * 
     * Function should only validate that ipnut is candidate values, that is numbers, 1 htrough 9. 
     * We are not lokking for aything else at the moment....
     *  
     *
     * @param arrayIndex the index of the array
     * @param oldValue   the value that was in the textfield before
     * @param newvalue   the value on the submitted number
     */

    private void numberSubmitted(int arrayIndex, String oldValue, String newvalue) {
    	if(this.gamePrep) {return;}
    	// We believe in second chances, this means that they all starts of as white and if the input is wrong they are marked red.
    	this.textfieldarray[arrayIndex].setStyle("-fx-control-inner-background: white");
    	
    	// If theres nothing to parse, then don't parse.
    	// An emtpy textfield is not a candidate to become correct either.
    	// Removal of a value means that other textfield which is candidate might become correct/valid
    	if(newvalue.equalsIgnoreCase("")) {
    		this.sudoku.removeSudokuValueInCell(arrayIndex, oldValue);
    		this.textfieldarray[arrayIndex].setCandidateValueFalse();
    		this.checkCandidateValues();
    		return ; 
    	}

        /*
         * Parse the newvalue from string to an int value
         * */
        int inputNumber;
        try {
            inputNumber = Integer.parseInt(newvalue);
        } catch (NumberFormatException ex) {
            this.setTexfieldWrongInput(arrayIndex, resources.getString(NOTVALID));
            this.textfieldarray[arrayIndex].setCandidateValueFalse();
            this.textfieldarray[arrayIndex].clear();
            LOGGER.log(Level.INFO, ex.toString(), ex);
            return;
        }

        /*
         * Verify that we are within our bounds
         * Allowed numbers are 1 through 9
         * */
        if (inputNumber > 9 || inputNumber < 1) {
        	this.setTexfieldWrongInput(arrayIndex, resources.getString("number"));
        	this.textfieldarray[arrayIndex].setCandidateValueFalse();
        	this.textfieldarray[arrayIndex].clear();
            return;
        }
        // All good. Insertion is might be possible
        this.setNumberInCell(arrayIndex, inputNumber);
    }

    /**
     * Used to validate and insert values to sudoku array using sudoku class
     * 
     * When return value for setSudokuValueInCell is true, this means that the game is complete
     * An animation is displayed in that case
     * 
     * On successfull insertion of sudoku value the textfield is not a candidate anymore, even if it was or was not.
     * 
     * We clear any feedback messages
     * 
     * Candidate values are supposedly only to be valid on removal of other textfields-value
     * But in case a removal doesn't happen before insertion of another number we want to check here 
     * 
     * In case the sudoku value is not valid we catch the thrown exception.
     * The value might be a candidatevalue thus setting the textfield candidate value to true
     * Display an appropriate message to the user
     * 
     * @param arrayIndex
     * @param inputNumber
     */
    private void setNumberInCell(int arrayIndex, int inputNumber) {
    	 // Check to see if the input follows Sudoku rules
        try {
            Boolean gameDone = this.sudoku.setSudokuValueInCell(arrayIndex, inputNumber);
            if(gameDone) {
            	LOGGER.info("Game is done");
            	this.winningAnimation();
            }
            this.feedbackLabel.setText("");
            this.textfieldarray[arrayIndex].setCandidateValueFalse();
            this.checkCandidateValues();
        } catch (BadNumberException ex) {
            LOGGER.log(Level.INFO, ex.toString(), ex);
            this.textfieldarray[arrayIndex].setCandidateValueTrue();
            this.setTexfieldWrongInput(arrayIndex, resources.getString(ex.getMessage()));
        }
    	   	
    }
    
    /**
     * Responsible to set textfield to red background
     * Sets the appropriate feedback to the user
     * @param textfieldIndex
     * @param feedback
     */
    private void setTexfieldWrongInput(int textfieldIndex, String feedback) {
    	this.feedbackLabel.setText(feedback);
    	this.textfieldarray[textfieldIndex].setStyle("-fx-control-inner-background: red");	
    }
    
    /**
     * Checks all the textfields if they are candidates to become valid
     * If they are a candidate we try to insert it's value
     * 
     */
    private void checkCandidateValues() {
    	for(int i=0; i< SUDOKULENGTH; i++) {
    		if (this.textfieldarray[i].getCandidateValue()) {
    			String val = this.textfieldarray[i].textProperty().getValue();
    			// Setting the value triggers a new input, thus validates it
    			this.numberSubmitted(i, "", val);	
    		}
    	}
    }


    /**
     * Reset the board to original state + remove listeners
     * Removing the listeners = listenerfunction + validation does not run on the autogenerated board
     * @deprecated
     */
    /*private void resetBoard() {
        for (int i = 0; i < textfieldarray.length; i++) {
      
            //textfieldarray[i].textProperty().removeListener(listeners[i]); //remove the listener
            listeners[i] = null;            //Zero out the object
            borderpane.requestFocus();        //Reset to default focus

            textfieldarray[i].setText("");  //Blank text
            textfieldarray[i].setDisable(false); //enable all the fields
            textfieldarray[i].setStyle("-fx-control-inner-background: white"); //white color
        }
    }*/

    /**
     * Insert the numbers for the new board
     * Call the sudoku function to give me number at the index
     * When we recieve -1 that field is for the user to enter number
     * <p>
     * All other should be set to the textfield. Also disable the field
     * so the user can't rig the game.
     */
    private void insertBoard() {
    	this.borderpane.requestFocus(); //Reset to default focus
        for (SudokuTextField field : this.textfieldarray) {
            try {
                int value = this.sudoku.getSudokuValueInCell(field.getIndex());

                if (value == -1) {
                	Platform.runLater(()->{
                		field.setText("");
                        field.setDisable(false);	
                	});
                    
                } else {
                	// Should use platform runlater but
                	// since we try to avoid listeners then this must be updated now before we 
                	// set gamePrep to false again.
                	//Platform.runLater(()->{
                		field.setText(String.format("%d", value));
                        field.setDisable(true);
                	//});
                    
                }

            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.toString(), e);
            }
        }
    }

    /**
     * Initialize the listeners
     * 
     */
    /*private void initializeListeners() {
        //Iterate through all the listeners and inilialize them on all of the textfields
        for (int i = 0; i < textfieldarray.length; i++) {
            final int index = i;
            listeners[i] = (observable, oldValue, newValue) -> numberSubmitted(index, oldValue, newValue);
            textfieldarray[i].textProperty().addListener(listeners[i]);
        }
    }*()

    /**
     * Animation when you complete the sudoku board
     */
    private void winningAnimation() {

        Bounds bounds = layout.getBoundsInLocal();
                            //Internationalized string
        Label label = new Label(resources.getString("congratz"));
                            //New transparent pane, to put on top of the board

        ImageView balloons1  = new ImageView( new Image("http://www.pngmart.com/files/1/Balloons-PNG-HD.png"));
        ImageView balloons2  = new ImageView( new Image("http://www.pngmart.com/files/1/Balloons-PNG-HD.png"));

                            //Resize the image.. needs more work
        balloons1.setFitWidth(bounds.getMaxX()/2);
        balloons2.setFitWidth(bounds.getMaxX()/2);

        balloons1.setFitHeight(bounds.getMaxY()/2);
        balloons2.setFitHeight(bounds.getMaxY()/2);

        Pane glass = new Pane();
                            //Add the label to the pane
        glass.getChildren().addAll(label, balloons1, balloons2);
                            //Position the label on the pane
        label.layoutXProperty().bind(glass.widthProperty().subtract(label.widthProperty()).divide(2));


                            //Make the pane transparent
        glass.setStyle("-fx-background-color: rgba(0, 0, 0, 0); -fx-background-radius: 10;");
                            //set the with and height of the transparent pane
        glass.setMaxWidth(borderpane.getMaxWidth());
        glass.setMaxHeight(borderpane.getMaxHeight());
                            //format the text on the label
        label.setStyle("-fx-font-size: 35px;-fx-text-fill: #FF9800; -fx-font-family: 'Comic Sans MS', cursive, sans-serif;");

                            //Add the pane to the underlaying stackpane
        layout.getChildren().addAll(glass);
        label.toFront();

                            //Hardcoded startvalues
        balloons1.relocate(0,0);
        balloons2.relocate(250, 0);

                            //Set the timeline
        Timeline timeline = new Timeline();
        KeyFrame heading = new KeyFrame(Duration.millis(10), event -> {
            label.setLayoutY(label.getLayoutY()+0.8);
            if(label.getLayoutY() > bounds.getMaxY()/2){
                layout.getChildren().removeAll(glass);
            }
        });

        KeyFrame ballongroup1 = new KeyFrame(Duration.millis(10), event -> balloons1.setLayoutY((balloons1.getLayoutY()+0.5)));
        KeyFrame ballongroup2 = new KeyFrame(Duration.millis(10), event -> balloons2.setLayoutY(balloons2.getLayoutY()+0.5));
                            //add the
        timeline.getKeyFrames().addAll(heading, ballongroup1, ballongroup2);
        timeline.setCycleCount(Timeline.INDEFINITE);
                            //start it
        timeline.play();
    }
}