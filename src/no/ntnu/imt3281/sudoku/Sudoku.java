package no.ntnu.imt3281.sudoku;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.sudoku.exceptions.BadNumberException;
import no.ntnu.imt3281.sudoku.enums.Difficulty;
import no.ntnu.imt3281.sudoku.enums.Status;



/**
 * Handles the logic that has to do with the Sudoku game
 * This stores the sudoku board in a one-dimensional array
 * All interaction to the sudoku array needs to go through helper functions provided
 * by this class.
 *
 * @author Joakim Ellestad, Magnus Lilja, Bendik Flobak
 *
 */
public class Sudoku {

	private static final Logger LOGGER = Logger.getLogger( Sudoku.class.getName() );
	/* Array for the sudoku board */
	private int[] board;				
	private static Status[] map = new Status[81];	//Array for each cell's status
	
	private int completeBoardCounter = 0;
	private int numbersFromStartCounter = 0;

	/**
	 * Initialize the base class with level.
	 * @param level
	 * @throws Exception
	 */
	public Sudoku(Difficulty level) throws Exception {
		
		IOHandle io = new IOHandle(level);
		try {
			board = io.getSudokuBoard();
		}catch(Exception e) {
            LOGGER.log( Level.SEVERE, e.toString(), e );
			//If we can't get the sudoku-board nothing else can run
			throw new Exception("Failed to initialize board");
		}
		
		//Count the number of already inserted numbers
        for (int aBoard : this.board) {
            if (aBoard > -1) {
                this.completeBoardCounter++;
            }
        }
        //Keeps track of numbers from start / initialization of board
		this.numbersFromStartCounter = this.completeBoardCounter;
	}
	
	
	/**
	 * Returns the value for the index passed in
	 * @param index
	 * @throws Exception if index is out of bounds.
	 */
	protected int getSudokuValueInCell(int index) throws Exception {
		if (index > -1 && index <= 80) {
			return board[index];
		}
		throw new Exception("indexInArray is out of bounds");	
	}
	
	/**
	 * 
	 * @return
	 * 
	 */
	protected int[] getBoard() {
		return this.board;
	}
	
	void setBoard(int[] board) {
		this.board = board;
	}

	/**
	 * Takes an index for the Sudoku board array and it's value to be set.
	 * Validates that it indeed can be set in that position.
	 * 
	 * Starts validating input in board
	 * Validates input in row
	 * Validates input in column
	 * 
	 * if validating passes set input to board array
	 * When the value is set, we also increment a global counter that when reaches 81 will 
	 * @throws BadNumberException if validation fails the function throws an exception with a user friendly message.
	 * 
	 * @param indexInBoardArray
	 * @param cellValue tests
	 * @return true when the game is complete. False when not complete
	 */
	protected Boolean setSudokuValueInCell(int indexInBoardArray, int cellValue) throws BadNumberException{
		if(!this.validateCellInSquareAtIndex(indexInBoardArray, cellValue)) {
			//cellValue can not be put in this block
			throw new BadNumberException(cellValue, "unvalidblock");
		}else if (!this.validateCellInRowAtIndex(indexInBoardArray, cellValue)) {
			throw new BadNumberException(cellValue, "unvalidrow");
		}else if(!this.validateCellInColumnAtIndex(indexInBoardArray, cellValue)) {
			throw new BadNumberException(cellValue, "unvalidcolumn");
		}
		this.board[indexInBoardArray] = cellValue;
		this.completeBoardCounter++;
		return (this.completeBoardCounter == 81);
	}
	
	/*
	 * Only remove if the old value is the value at the array position
	 */
	protected void removeSudokuValueInCell(int indexInBoardArray, String oldValue) {
		int ov;
		try {
			ov = Integer.parseInt(oldValue);
		}catch (NumberFormatException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
			return;
		}
		if (this.board[indexInBoardArray] == ov) {
			this.board[indexInBoardArray] = -1;
			if (this.completeBoardCounter > numbersFromStartCounter) {
	            this.completeBoardCounter--;
	        }
		}
		
	}
	
	/**
	 * Mirrors the board on the horizontal axis by reversing the "rows" in the board array.
	 */
	void mirrorHorizontal() {
		for (int row = 0; row < board.length/9/2; row++) {
			for (int column = 0; column < board.length/9; column++) {
				int temp = board[row*9+column];
				board[row*9+column] = board[(8-row)*9+column];
				board[(8-row)*9+column] = temp;
			}
		}
	}
	
	/**
	 * Mirrors the board on the vertical axis.
	 * Reverses each row by first finding row and then doing the same for loop as in the horizontal mirroring on each row.
	 */
    void mirrorVertical() {
		for (int row = 0; row < this.board.length; row+=9) {
			for (int column = 0; column < (this.board.length/9)/2; column++) {
				int temp = this.board[row+column];
				this.board[row+column] = this.board[row+8-column];
				this.board[row+8-column] = temp;
			}
		}
	}
	
	/**
	 * Mirrors the board on the descending diagonal
	 * Uses a temp array to make rows into columns and then inserts into board.
	 * row=column, vice versa.
	 */
    void mirrorDescendingDiagonal() {
		int[] temp = new int[81];
		for (int row = 0; row < board.length/9; row++) {
			for (int column = 0; column < board.length/9; column++) {
				temp[column*9+row] = board[row*9+column];
			}
		}
		board = temp;
	}
	
	/**
	 * Mirrors the board on the ascending diagonal
	 */
    void mirrorAscendingDiagonal() {
		int[] temp = new int[81];
		for (int row = 0; row < board.length/9; row++) {
			for (int column = 0; column < board.length/9; column++) {
				temp[(8-column)*9+8-row] = board[row*9+column];
			}
		}
		board = temp;
	}
	
	/**
	 * Validates  row
	 * Takes a cells index and validates the value for the position.
	 * 
	 * A row is 
	 * 
	 * @param cellIndex
	 * @return true/false based on logic
	 */

    Boolean validateCellInRowAtIndex(int cellIndex, int cellValue) {
		ArrayList<Integer> rowArray = new ArrayList<>();
		/*
	    * Finds the start of the Row (cellIndex/9*9), i.e. in integer 17/9 = 1, 1*9 = 9
	    */
		int startOfRow = cellIndex/9*9;
		
		/*
		 * Starts at the start of the row and moves along the row by incrementing
		 * The roof of the for loop is startOfRow + 9 because we need to move through all 9 cells.v.
		 */
		for (int i = startOfRow; i < startOfRow+9; i++) {
			rowArray.add(this.board[i]);
		}

		Iterator<Integer> itr = rowArray.iterator();

		/*
		 * Iterates the array and checks if the input value is present in the column.
		 */
		while (itr.hasNext()) {
			int number = itr.next();
			if(cellValue == number) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validates column
	 * Takes a cells index and it's value and validates it for the position.
	 * 
	 * @param cellIndex
	 * @param cellValue
	 * @return
	 */


	Boolean validateCellInColumnAtIndex(int cellIndex, int cellValue) {
		ArrayList<Integer> columnArray = new ArrayList<>();
			/*
		    * Finds the start of the Column (cellIndex%9)
		    */
		int startOfColumn = cellIndex%9;
		
		/*
		 * Starts at the start of the column and moves down the column by adding 9 each time.
		 * The roof of the for loop is startOfColumn+9*9, this will leave us with the 9 indexes needed.
		 */
		for (int i = startOfColumn; i < startOfColumn+9*9; i+=9) {
			columnArray.add(this.board[i]);
		}

		Iterator<Integer> itr = columnArray.iterator();

		/*
		 * Iterates the array and checks if the input value is present in the column.
		 */
		while (itr.hasNext()) {
			int number = (int) itr.next();
			if(cellValue == number) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validates square
	 * Takes a cells index and it's value and validates it for the position.
	 * @param cellIndex
	 * @param cellValue
	 * @return
	 */
	Boolean validateCellInSquareAtIndex(int cellIndex, int cellValue) {

		ArrayList<Integer> blockArray =  new ArrayList<>();
		
		int row_coordinate = cellIndex/9;
		int column_coordinate = cellIndex%9;
		
		int row_start = row_coordinate - (row_coordinate%3);
		int column_start = column_coordinate - (column_coordinate%3);
		int row_stop = row_start+2;
		int column_stop = column_start+2;
		for(int r = row_start; r<=row_stop; r++) {
			for(int c = column_start; c <=column_stop;c++) {
				blockArray.add(this.board[((r*9)+c)]);
			}
		}
		Iterator<Integer> itr = blockArray.iterator();
		while(itr.hasNext()) {
			if(cellValue == (int) itr.next()) {
				return false;
			}
		}
		
		return true;
	}
	
	//Getter for a map cell
	public static Status getMap(int i) {
		return map[i];
	}


	//Setter for a map cell
	public static void setMap(int i, Status map) {
		Sudoku.map[i] = map;
	}

	// ########### SOME FUNCTIONALITY FOR THIS CLASS ###########
	/*
       Receive a difficulty-rating for the new board from the controller class
       call for IOHandle to read a new board from the JSON file
       Rotate the board
       Return the perfectly completed board to the controller class

       Keep track of the status for each cell --> controller class calls for a function in this class to set a new
       number in a cell (return bool back to controller?)

       Reset function when the user wants a new board


	 */
    // ########################################################
}
